module Geohash
  class Decoder

    def self.decode(geohash)
      instance = new(geohash)
      instance.decode
    end

    def initialize(geohash)
      @geohash = geohash
    end

    def decode
      decimal_values = @geohash.chars.map { |char| convert_to_decimal(char) }
      p decimal_values
      bit_array      = decimal_values.map { |dec|  convert_to_binary( dec ) }.flatten

puts "DEBUG: bit_array #{bit_array.join('')}"
puts "DEBUG: should be 0110111111110000010000010"

      latitude_bits  = []
      longitude_bits = []

      bit_array.each_with_index do |bit, index|
        if index % 2 == 0
          latitude_bits << bit
        else
          longitude_bits << bit
        end
      end

puts "DEBUG: latitude_bits #{latitude_bits.join("")}"
puts "DEBUG: should be     101111001001"
puts "DEBUG: longitude_bits #{longitude_bits.join("")}"
puts "DEBUG: should be      0111110000000"
      []
    end

    def latitude
      nil
    end

    def longitude
      nil
    end

    private

    def convert_to_decimal(char)
      {
        '0'  => 0,
        '1'  => 1,
        '2'  => 2,
        '3'  => 3,
        '4'  => 4,
        '5'  => 5,
        '6'  => 6,
        '7'  => 7,
        '8'  => 8,
        '9'  => 9,
        'b'  => 10,
        'c'  => 11,
        'd'  => 12,
        'e'  => 13,
        'f'  => 14,
        'g'  => 15,
        'h'  => 16,
        'j'  => 17,
        'k'  => 18,
        'm'  => 19,
        'n'  => 20,
        'p'  => 21,
        'q'  => 22,
        'r'  => 23,
        's'  => 24,
        't'  => 25,
        'u'  => 26,
        'v'  => 27,
        'w'  => 28,
        'x'  => 29,
        'y'  => 30,
        'z'  => 31
      }[char]
    end

    def convert_to_binary(decimal)
      decimal.to_s(2).split('')
    end
  end
end

