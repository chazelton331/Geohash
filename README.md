This is a small Ruby application to return the latitude and longitude point from a given geohash string.

For reference: http://en.wikipedia.org/wiki/Geohash

Usage:

```
require File.join(File.dirname(__FILE__), "path", "to", "decoder")
Geohash::Decoder.decode("ezs42") # returns an array that contains the latitude and longitude  [ 42.6, -5.6 ]
```